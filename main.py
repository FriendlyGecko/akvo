from datetime import datetime
from tkinter import *
import csv


# The Main Window:
window = Tk()
window.title('Akvo: Simple Water Tracker')
window.configure(background="black")
window.minsize(320, 512)
window.geometry('320x512+0+0')

# Important Variables
current_date = str(datetime.now().date().toordinal())
water_history = {}
user_pref = {}
daily_percent = 0.0


# These are the functions
# This sets up the program so that it can make entries for today
def update_date():
    global current_date
    current_date = str(datetime.now().date().toordinal())
    with open('water_history.csv', newline='') as history:
        global water_history
        reader = csv.DictReader(history)
        for row in reader:
            water_history[row['date']] = int(row['amount'])


# This loads the history from the CSV file and updates it as a running dictionary
def load_history():
    with open('water_history.csv', newline='') as history:
        global water_history
        reader = csv.DictReader(history)
        for row in reader:
            water_history[row['date']] = int(row['amount'])


# This loads the user's preferences from the CSV and updates a running dictionary
def load_user_pref():
    with open('user_pref.csv', newline='') as pref:
        global user_pref
        reader = csv.DictReader(pref)
        for row in reader:
            user_pref[row['option']] = int(row['value'])


# This updates the percent of the daily goal drank in order to properly display the right visual
def update_daily_percent():
    global daily_percent
    daily_percent = water_history.get(current_date, 0) / user_pref['goal']


# This determines which image to display
def update_visual():
    if daily_percent == 0.0:
        canvas.create_image(0, 0, image=empty, anchor=NW)
    elif 0.25 > daily_percent > 0.0:
        canvas.create_image(0, 0, image=little, anchor=NW)
    elif 0.5 > daily_percent >= 0.25:
        canvas.create_image(0, 0, image=quarter, anchor=NW)
    elif 0.75 > daily_percent >= 0.5:
        canvas.create_image(0, 0, image=half, anchor=NW)
    elif 1.0 > daily_percent >= 0.75:
        canvas.create_image(0, 0, image=threeQuarter, anchor=NW)
    elif daily_percent >= 1.0:
        canvas.create_image(0, 0, image=full, anchor=NW)


# Updates and saves the user preference CSV file with new settings
def update_user_pref():
    global user_pref
    with open('user_pref.csv', 'w') as pref:
        writer = csv.writer(pref)
        writer.writerow(['option', 'value'])
        for row in user_pref.items():
            writer.writerow(row)


# This is the main function that allows the user to update their water intake
def water_update(amount):
    global water_history
    global current_date
    update_date()
    load_history()
    water_history[current_date] = water_history.get(current_date, 0) + int(amount)
    with open('water_history.csv', 'w') as history:
        writer = csv.writer(history)
        writer.writerow(['date', 'amount'])
        for row in water_history.items():
            writer.writerow(row)
    update_daily_percent()
    update_visual()


# This gives functionality to the submit button
def click_submit():
    try:
        water_update(int(intake_entry.get()))
        intake_entry.delete(0, 0)
    except ValueError:
        return 'Use an integer.'


# Gives function to fast update based on the user's preferences
def fast_update():
    water_update(user_pref.get('quick_update', 8))


# Initial start up functions
update_date()
load_history()
load_user_pref()


# Display new window with settings for the user to change
def click_settings():
    global user_pref
    # Settings Window:
    settings = Toplevel()
    settings.title('Akvo Settings')
    settings.configure(background="black")
    settings.minsize(320, 512)
    settings.geometry('320x512+0+0')

    def click_update():
        global user_pref
        user_pref['unit'] = int(str(unit_var)[-1])
        user_pref['goal'] = int(goal_entry.get())
        user_pref['quick_update'] = int(quick_entry.get())
        update_user_pref()
        update_daily_percent()
        update_visual()

    # This sets a variable for the unit settings
    unit_var = IntVar()

    # This creates the different options to be shown:
    # This creates the radio button choices for unit choice
    unit_oz = Radiobutton(settings, text='Fluid Ounces', variable=unit_var, value=0)
    unit_ml = Radiobutton(settings, text='Milliliters', variable=unit_var, value=1)
    # This creates a goal entry box
    goal_entry = Entry(settings, width=6, borderwidth=5, bg='grey')
    # This creates a quick update entry box
    quick_entry = Entry(settings, width=6, borderwidth=5, bg='grey')
    # This creates a button to actually call the update preferences function
    update_settings = Button(settings, text='Update Preferences', width=15, command=click_update)

    # This places and labels the options
    Label(settings, text="Units", bg='black', fg='grey', font='none 12 bold').grid(row=0, column=0, sticky=E)
    unit_oz.grid(row=1, column=0, sticky=W)
    unit_ml.grid(row=1, column=1, sticky=E)
    Label(settings, text="Daily Goal", bg='black', fg='grey', font='none 12 bold').grid(row=2, column=0, sticky=E)
    goal_entry.grid(row=3, column=0, sticky=E)
    Label(settings, text="Quick Entry", bg='black', fg='grey', font='none 12 bold').grid(row=4, column=0, sticky=E)
    quick_entry.grid(row=5, column=0, sticky=E)
    update_settings.grid(row=7, column=0, sticky=E)

    # This selects/auto-populates the radio button/entry boxes so the user knows the current configuration
    if user_pref['unit'] == 0:
        unit_oz.select()
    elif user_pref['unit'] == 1:
        unit_ml.select()
    goal_entry.insert(0, str(user_pref['goal']))
    quick_entry.insert(0, str(user_pref['quick_update']))


# Displays new window that displays user history
def click_history():
    global water_history
    # History Window:
    history = Toplevel()
    history.title('Intake History')
    history.configure(background="black")
    history.minsize(320, 512)
    history.geometry('320x512+0+0')

    # Shows user their full history
    history_entry = Text(history, width=42, height=12, borderwidth=5, bg='grey')
    history_entry.grid(row=0, column=0, sticky=E)
    for entry in water_history:
        history_entry.insert('0.0', ' \n')
        history_entry.insert('0.0', str(water_history[entry]))
        history_entry.insert('0.0', str(datetime.fromordinal(int(entry)).date()) + '                             ')


# This provides a label for context for the display
Label(window, text="Left Towards Goal", bg='black', fg='grey', font='none 12 bold').grid(row=1, column=0, sticky=E)

# This creates and places the intake entry field
intake_entry = Entry(window, width=6, borderwidth=5, bg='grey')
intake_entry.grid(row=3, column=0, sticky=W)

# This creates the canvas
canvas = Canvas(window, width=300, height=300)

# This creates the visual display of current status
little = PhotoImage(file="./sources/lessThanQuarter.gif")
quarter = PhotoImage(file="./sources/quarter.gif")
half = PhotoImage(file="./sources/half.gif")
threeQuarter = PhotoImage(file='./sources/threeQuarter.gif')
full = PhotoImage(file='./sources/full.gif')
empty = PhotoImage(file="./sources/empty.gif")

# This is the container for the visual display
canvas.grid(row=2, column=0, columnspan=3)

# This creates the buttons.
submit_button = Button(window, text='Submit', width=10, command=click_submit)
q_button = Button(window, text='Fast Update', width=0, command=lambda: water_update(user_pref.get('quick_update', 8)))
history_button = Button(window, text='History', width=10, command=click_history)
settings_button = Button(window, text='Settings', width=10, command=click_settings)

# This places the buttons
submit_button.grid(row=3, column=0, sticky=E)
q_button.grid(row=3, column=1, sticky=S)
history_button.grid(row=0, column=0, sticky=NW)
settings_button.grid(row=0, column=1, sticky=NE)

# Latter start up functions
update_daily_percent()
update_visual()

# Runs the program:
window.mainloop()
