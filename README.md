## Akvo

## What is it?
A Python based water consumption tracker built for mobile Linux.

Akvo utilizes datetime to use today's date a key in a dictionary where the total amount drank in the day is the value for said key.
Afterwards it saves it as a CSV file and can be loaded at a later date, showing historical water consumption data.

This project is mainly acting as a study for the creation of a calorie tracker.

## Installation
At this time there isn't one, simply put all the files into the same folder and run Tracker.py using python3.

## Usage
Input the number of ounces or milliliters ingested click submit to update today's total. Use quick update to add an amount set in user preferences.
